from rest_framework import viewsets, permissions
from .serializers import LedgerSerializer
from .models import Ledger

class LedgerViewSet(viewsets.ModelViewSet):
    """
    :)
    """
    queryset = Ledger.objects.all()
    serializer_class = LedgerSerializer
    permission_classes = [permissions.IsAuthenticated]
