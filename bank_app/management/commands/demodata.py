from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from bank_app.models import Account, Employee, Customer

class Command(BaseCommand):
    def handle(self, **options):
        print('Adding data to the bank...')

        # Adding a superuser
        # User.objects.create_superuser('admin', '', 'admin1234')

        # Add the bank
        if not User.objects.filter(username='bank').exists():
            user = User.objects.create_user(
                username='bank',
                first_name='Bank',
                last_name='Bank',
                email='bank@bank.com',
                password='bank1234'
            )
            customer = Customer()
            customer.phone = '12345678'
            customer.user = user
            customer.rank = 'Gold'
            customer.save()

            bank_user = User.objects.get(username='bank')
            Account.create('bank account', bank_user.pk, False)
            bank_account = Account.objects.get(name='bank account')

            # Add money to the bank
            customer.create_loan('Money to bank', bank_user, 10000000, bank_account.pk)

        # Add an employee
        if not User.objects.filter(username='employee').exists():
            user = User.objects.create_user(
                username='employee',
                first_name='Employee',
                last_name='Employee',
                email='emp@emp.com',
                password='employee1234'
            )
            employee = Employee()
            employee.phone = '12345678'
            employee.user = user
            employee.save()
