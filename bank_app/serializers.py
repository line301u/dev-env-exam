from rest_framework import serializers
from bank_app.models import Ledger

class LedgerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ledger
        fields = ['account', 'amount', 'timestamp', 'text', 'transaction_id']
