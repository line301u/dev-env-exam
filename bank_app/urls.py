from django.urls import path
from . import views

app_name = 'bank_app'

urlpatterns = [
    path('home', views.index, name='index'),
    path('customer', views.index, name='customer'),
    path('employee', views.index, name='employee'),
    path('get_customers_partial', views.get_customers_partial, name='get_customers_partial'),
    path('create_customer', views.create_customer, name='create_customer'),
    path('customer_details/<int:pk>', views.customer_details, name='customer_details'),
    path('create_account', views.create_account, name='create_account'),
    path('get_accounts_partial/<int:pk>', views.get_accounts_partial, name='get_accounts_partial'),
    path('set_rank', views.set_rank, name='set_rank'),
    path('create_transfer', views.create_transfer, name='create_transfer'),
    path('create_loan', views.create_loan, name='create_loan'),
    path('account_details/<uuid:pk>', views.account_details, name='account_details'),
    path('get_account_movements_partial/<uuid:pk>', views.get_account_movements_partial, name='get_account_movements_partial'),
    path('logout', views.logout, name='logout'),
    path('test_me', views.test_me, name="test_me"),
    path('api/external_transfer', views.external_transfer, name="external_transfer"),
]
