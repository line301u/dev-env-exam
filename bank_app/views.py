from django.shortcuts import render, get_object_or_404
from .models import Account, Customer, Employee, Ledger
from django.contrib.auth import logout as dj_logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.conf import settings

# TEST MEEEEEEEE
import requests

# REST API
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse


@api_view(['POST'])
def external_transfer(request):
    if request.method == 'POST':
        print("hello world")
        transfer = JSONParser().parse(request)

        amount = transfer['amount']
        from_account_id = transfer['from_account_id']
        to_account_id = transfer['to_account_id']
        from_text = transfer['from_text']
        to_text = transfer['to_text']

        Ledger.internal_transfer(amount, from_account_id, to_account_id, from_text, to_text)
        return JsonResponse("hi mom its me POST", safe=False)


def test_me(request):
    response = requests.get("http://127.0.0.1:4321/ledgers", auth=('a', 'kage1234'))
    ledgers = response.json()
    return render(request, 'bank_app/test_me.html', {'ledgers': ledgers})


def index(request):
    user_id = request.user.pk

    if request.user.is_authenticated:
        if not request.user.is_verified():
            # user not logged in using two-factor
            return HttpResponseRedirect(reverse('two_factor:setup'))

        # if user is employee, should not access /customer.html
        if Customer.objects.filter(user=user_id).exists():
            return render(request, 'bank_app/customer.html', {'customer': request.user.customer, 'bank_registration_number': settings.BANK_REGISTRATION_NUMBER})
        elif Employee.objects.filter(user=user_id).exists():
            return render(request, 'bank_app/employee.html', {})
        else:
            print(Customer.objects.all())
            print('logging out')
            logout(request)
            return HttpResponseRedirect(reverse('two_factor:login'))

    else:
        return HttpResponseRedirect(reverse('two_factor:login'))

@login_required
def customer_details(request, pk):
    customer = get_object_or_404(Customer, pk=pk)
    return render(request, 'bank_app/customer_details.html', {'customer': customer})

@login_required
def get_customers_partial(request):
    customers = Customer.objects.all()
    return render(request, 'bank_app/customers_list_partial.html', {'customers': customers})

@login_required
def create_customer(request):
    username = request.POST['username']
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']
    phone = request.POST['phone']
    email = request.POST['email']
    password = request.POST['password']

    customer = Customer.create(username, first_name, last_name, phone, email, password)
    response = render(request, 'bank_app/customers_list_partial.html', {'customers': [customer]})
    return response

@login_required
def get_accounts_partial(request, pk):
    accounts = Account.objects.filter(user=pk)
    return render(request, 'bank_app/accounts_list_partial.html', {'accounts': accounts})

@login_required
def create_account(request):
    name = request.POST['name']
    pk = request.POST['pk']
    is_loan = request.POST.get('is_loan', False)
    account = Account.create(name, pk, is_loan)
    response = render(request, 'bank_app/accounts_list_partial.html', {'accounts': [account]})
    return response

@login_required
def set_rank(request):
    pk = request.POST['pk']
    rank = request.POST['rank']
    customer = get_object_or_404(Customer, pk=pk)
    customer.set_rank(rank)
    response = render(request, 'bank_app/rank_partial.html', {'rank': rank})
    return response

@login_required
def create_transfer(request):
    from_account_id = request.POST['from_account_id']
    from_text = request.POST['from_text']

    to_account_id = request.POST['to_account_id']
    to_text = request.POST['to_text']
    to_registraton_number = request.POST['to_registraton_number']

    amount = float(request.POST['amount'])
    internal_bank_registration_number = settings.BANK_REGISTRATION_NUMBER
    banks = settings.BANKS

    context = {
        'customer': request.user.customer,
    }

    if not any(bank['registration_number'] == to_registraton_number for bank in banks):
        context['message'] = {
            'text': 'Account does not exist',
            'type': 'error'
        }
        response = render(request, 'bank_app/create_transfer_partial.html', context)
    elif to_registraton_number == internal_bank_registration_number:
        Ledger.internal_transfer(amount, from_account_id, to_account_id, from_text, to_text)
        context['message'] = {
            'text': 'Your money was transfered successfully',
            'type': 'success'
        }
        response = render(request, 'bank_app/create_transfer_partial.html', context)
        response.headers["HX-Trigger"] = "accountUpdate"
    else:
        print("Other bank")
        Ledger.external_transfer(amount, from_account_id, to_account_id, from_text, to_text, to_registraton_number)
        context['message'] = {
            'text': 'Your money was transfered successfully',
            'type': 'success'
        }
        response = render(request, 'bank_app/create_transfer_partial.html', context)
        response.headers["HX-Trigger"] = "accountUpdate"

    return response

@login_required
def create_loan(request):
    amount = float(request.POST['amount'])
    name = request.POST['loan_name']
    to_account_id = request.POST['to_account_id']
    pk = request.user.pk

    # Can they make a loan?
    if request.user.customer.can_make_loan:
        # If yes - make loan
        request.user.customer.create_loan(name, pk, amount, to_account_id)
    # Else return error message
    response = render(request, 'bank_app/customer.html', {'customer': request.user.customer})
    return response

@login_required
def account_details(request, pk):
    account = get_object_or_404(Account, pk=pk)
    is_employee = Employee.objects.filter(user=request.user.pk).exists()
    is_owner_of_account = request.user.pk == account.user.pk

    if not is_owner_of_account and not is_employee:
        return HttpResponseRedirect(reverse('bank_app:index'))

    return render(request, 'bank_app/account_details.html', {'account': account})

@login_required
def get_account_movements_partial(request, pk):
    account = get_object_or_404(Account, pk=pk)
    movements = account.movements

    return render(request, 'bank_app/account_movements_partial.html', {'movements': movements})

@login_required
def logout(request):
    dj_logout(request)
    return HttpResponseRedirect(reverse('login'))
