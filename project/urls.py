from django.contrib import admin
from django.urls import path, include
from two_factor.views import LoginView, SetupView
from two_factor.urls import urlpatterns as tf_urls
from bank_app.apis import LedgerViewSet

# REST API
from rest_framework import routers
router = routers.DefaultRouter()
router.register(r'ledgers', LedgerViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('bank/', include('bank_app.urls')),
    # 2FA
    path('', include(tf_urls)),
    path('', LoginView.as_view(), name='login'),
    path('setup/', SetupView.as_view(), name='setup'),
    # REST API
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
